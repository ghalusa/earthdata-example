<?php

/**
 * @file
 * Contains \Drupal\earthdata_example\Processors\ProcessorInterface.
 */

namespace Drupal\earthdata_example\Processors;

interface ProcessorInterface {

  /**
   * Do Stuff
   * @return array
   *
   */
  public function doStuff(array $some_array, string $some_string);

}
