# Earthdata Example Module

## Contents of this File

* Introduction
* Installation
* Maintainers

### Introduction

An example module illustrating basic module architecture.

### Installation

* Install as you would normally install a contributed Drupal module.
   See: [Installing Drupal 8 Modules](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules) for further information.

### Maintainers/Support

* [SSAI](https://www.ssaihq.com/)
* Goran Halusa: [goran.halusa@nasa.gov](goran.halusa@nasa.gov)