<?php

/**
 * @file
 * Contains preprocess.inc
 *
 * Available variables:
 * - $example_variable_1
 * - $example_variable_2
 */

function earthdata_example_preprocess_bar_page(&$variables) {

  dd($variables['example_variable_1']);
  dd($variables['example_variable_2']);

  // Processing goes here.
  $bar = json_encode(array(rawurlencode('some variable\'s value')), JSON_HEX_QUOT | JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS);

  // Output variables.
  $variables['foo'] = 'test foo';
  $variables['bar'] = $bar;

  // dd($variables);
}